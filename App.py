#import itertools

import dash
from dash import dcc
from dash import html
import dash_cytoscape as cyto
from dash.dependencies import Input, Output
import plotly.express as px

import itertools
import json

import keyhelperfunctions
import track
import trackloader
import sqldatabase

if __name__ == "__main__":
 
    # create the db
    store_in_memory = True # store in memory while we test
    db = sqldatabase.SQLDatabase(store_in_memory)
    db.createTracksTable()

    # load tracks
    folder = "D:\\Music (Sorted)\\Mix\\_Compilations"
    loader = trackloader.TrackLoader()
    tracks = loader.loadTracksFromFolder(folder)

    # insert tracks into db
    db.insertTracks(tracks)

    # clean up tracks
    del tracks 

    tracks_out = db.selectAllTracks()
    print([str(t) for t in tracks_out])

    # generate edges based on compatible keys
    transitions = [track.Transition.generateId(t1, t2) for t1, t2 in itertools.combinations(tracks_out, r=2) if keyhelperfunctions.KeysAreCompatible(t1._key, t2._key)]
    graph_elements = [t.toNode() for t in tracks_out] + [t.toEdge() for t in transitions]

    cyto.load_extra_layouts()
    app = dash.Dash(__name__)

    app.layout = html.Div(style={"backgroundColor": '#222227', "font-family": "sans-serif"},
        children = 
        [
            html.Pre(id='hover', style = {"textAlign" : "left", "color" : "#FFFFFF"}),
            cyto.Cytoscape(
                id = "my_graph",
                layout = {"name" : "cose"},
                #style = {"width" : "100%", "height" : "100%"},
                elements = graph_elements)
        ]
    )

    @app.callback(Output('hover', 'children'), Input('my_graph', 'mouseoverNodeData'))
    def displayTapNodeData(data):
        if data:
            #return f"{data['title']}\n{data['artist']}\n"
            return json.dumps(data, indent=2)

    app.run_server(debug=True)
