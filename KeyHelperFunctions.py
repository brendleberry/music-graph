# map of Traktor keys to key strings
key_conv = {
    "1d" : ["C", "Cmaj"],                       "1m" : ["Am", "Amin"],
    "2d" : ["G", "Gmaj"],                       "2m" : ["Em", "Emin"],
    "3d" : ["D", "Dmaj"],                       "3m" : ["Bm", "Bmin"],
    "4d" : ["A", "Amaj"],                       "4m" : ["F#m", "F#min", "Gbm", "Gbmin"],
    "5d" : ["E", "Emaj"],                       "5m" : ["C#m", "C#min", "Dbm", "Dbmin"],
    "6d" : ["B", "Bmaj"],                       "6m" : ["G#m", "G#min", "Abm", "Abmin"],
    "7d" : ["F#", "F#maj", "Gb", "Gbmaj"],      "7m" : ["D#m", "D#min", "Ebm", "Ebmin"],
    "8d" : ["C#", "C#maj", "Db", "Dbmaj"],      "8m" : ["A#m", "A#min", "Bbm", "Bbmin"],
    "9d" : ["G#", "G#maj", "Ab", "Abmaj"],      "9m" : ["Fm", "Fmin"],
    "10d" : ["D#", "D#maj", "Eb", "Ebmaj"],     "10m" : ["Cm", "Cmin"],
    "11d" : ["A#", "A#maj", "Bb", "Bbmaj"],     "11m" : ["Gm", "Gmin"],
    "12d" : ["F", "Fmaj"],                      "12m" : ["Dm", "Dmin"]
}

# convert key string to Traktor format
def ConvertKey(keystr:str):
    for key, val in key_conv.items():
        if keystr == key:
            # key is already in correct format
            return key
        elif keystr in val:
            # convert to Traktor key format
            return key

    # key is not recognised
    return None
    # raise(RuntimeError(f"Unrecognised key string: {keystr}"))

# get tracks that can be mixed in key
def GetAdjacentKeys(key:str) -> list[str]: 
    num = key[:-1]
    key_type = key[-1]
    inv_key_type = "m" if key_type == "d" else "d"

    # bit of modular arithmetic
    down_one_key_num = "12" if ((int(num) - 1) % 12 == 0) else str((int(num) - 1) % 12)
    up_one_key_num = "12" if ((int(num) + 1) % 12 == 0) else str((int(num) + 1) % 12)
    
    down_one_key = down_one_key_num + key_type
    up_one_key = up_one_key_num + key_type
    
    relative_maj_or_min = num + inv_key_type

    return [key, relative_maj_or_min, up_one_key, down_one_key]

def KeysAreCompatible(key1:str, key2:str):
    return key1 in GetAdjacentKeys(key2)