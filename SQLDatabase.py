import sqlite3
import track

class SQLDatabase:

    def __init__(self, store_in_memory):
        self._connection = sqlite3.connect(":memory:") if store_in_memory else sqlite3.connect("musicgraph.db")

    def __del__(self):
        self._connection.close()

    def createTracksTable(self):
        c = self._connection.cursor()
        c.execute("CREATE TABLE IF NOT EXISTS tracks(id TEXT NOT NULL PRIMARY KEY, title TEXT NOT NULL, artist TEXT NOT NULL, key TEXT)")
        self._connection.commit()
        c.close()

    def insertTrack(self, track):
        c = self._connection.cursor()
        c.execute("INSERT INTO tracks VALUES (?,?,?,?)", (track._id, track._title, track._artist, track._key))
        self._connection.commit()
        c.close()

    def insertTracks(self, tracks):
        rows = [(t._id, t._title, t._artist, t._key) for t in tracks] # convert to rows
        c = self._connection.cursor()
        c.executemany("INSERT INTO tracks VALUES (?,?,?,?)", rows)
        self._connection.commit()
        c.close()

    def selectTrackById(self, track_id):
        c = self._connection.cursor()
        c.execute("SELECT * FROM tracks WHERE id = ?", (track_id,))
        rows = c.fetchall()
        c.close()

        assert len(rows) == 1
        return track.Track(*rows[0])

    def selectAllTracks(self):
        c = self._connection.cursor()
        c.execute("SELECT * FROM tracks")
        rows = c.fetchall()
        c.close()

        return [track.Track(*row) for row in rows]

    def createTransitionsTable(self):
        c = self._connection.cursor()
        c.execute("CREATE TABLE IF NOT EXISTS transitions(id TEXT NOT NULL PRIMARY KEY, first_track_id TEXT NOT NULL, second_track_id TEXT NOT NULL)")
        self._connection.commit()
        c.close()

    def insertTransition(self, transition):
        c = self._connection.cursor()
        c.execute("INSERT INTO transitions VALUES (?,?,?)", (transition._id, transition._first_track_id, transition._second_track_id))
        self._connection.commit()
        c.close()

    def selectTransitionById(self, transition_id):
        c = self._connection.cursor()
        c.execute("SELECT * FROM transitions WHERE id = ?", (transition_id,))
        rows = c.fetchall()
        c.close()

        assert len(rows) == 1
        return track.Transition(*rows[0])

    def selectAllTransitions(self):
        c = self._connection.cursor()
        c.execute("SELECT * FROM transitions")
        rows = c.fetchall()
        c.close()

        return [track.Transition(*row) for row in rows]
        