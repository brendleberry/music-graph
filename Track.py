import uuid
from hashlib import md5
import keyhelperfunctions

class Track:

    def __init__(self, id:str, title:str, artist:str, key:str):
        self._id = id
        self._title = title
        self._artist = artist
        self._key = key

    @classmethod
    def generateId(cls, title:str, artist:str, key:str):

        # give the track a reproducible uuid by using its title and artist as a seed
        m = md5()
        m.update(f"{artist}-{title}".encode("utf-8"))
        id = str(uuid.UUID(m.hexdigest()))

        return cls(id, title, artist, key)

    def __eq__(self, rhs):
        return (self.__dict__ == rhs.__dict__) if isinstance(self, rhs.__class__) else False

    def __ne__(self, rhs):
        return not self.__eq__(rhs)

    def __str__(self):
        return str(self.__class__) + ": " + str(self.__dict__)

    def toNode(self):
        return { "data" : {"id" : self._id, "title" : self._title, "artist" : self._artist, "key" : self._key}, "position": {"x" : 0, "y" : 0} }

class Transition:

    def __init__(self, id:str, first_track_id:str, second_track_id:str):
        self._id = id
        self._first_track_id = first_track_id
        self._second_track_id = second_track_id

    @classmethod
    def generateId(cls, first_track:Track, second_track:Track):

        # just XOR the two track IDs together to generate the transition ID
        # this should prevent us adding multiple transitions between the same track
        assert(first_track._id != second_track._id)
        transition_id = "".join([chr(ord(a) ^ ord(b)) for a,b in zip(first_track._id, second_track._id)]) # need to fix this

        return cls(transition_id, first_track._id, second_track._id)
    
    def __eq__(self, rhs):
        return (self.__dict__ == rhs.__dict__) if isinstance(self, rhs.__class__) else False

    def __ne__(self, rhs):
        return not self.__eq__(rhs)

    def toEdge(self):
        return {'data': {"id" : self._id, 'source': self._first_track_id, 'target': self._second_track_id}}

# # def getLabel(self):
# #     return f"{self._artist} - {self._title} ({self._key})"