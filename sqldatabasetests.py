import sqldatabase
import track
import unittest

class SQLDatabaseTests(unittest.TestCase):
    
    # test data
    track1 = track.Track.generateId("title1", "artist1", "key1")
    track2 = track.Track.generateId("title2", "artist2", "key2")
    tracks = [track1, track2]
    transition1 = track.Transition.generateId(track1, track2)
    # transition2 = track.Transition.generateId(track_2, track_1) # might be the same ID as the first???

    def testInsertAndSelectSingleTrack(self):
        # make the database (store in memory only)
        store_in_memory = True
        sql_db = sqldatabase.SQLDatabase(store_in_memory)
        sql_db.createTracksTable()

        # insert tracks into db individually
        sql_db.insertTrack(self.track1)
        sql_db.insertTrack(self.track2)
        
        # select first track and check the result
        track1_from_db = sql_db.selectTrackById(self.track1._id)        
        self.assertEqual(track1_from_db, self.track1)

        # select second track and check the result
        track2_from_db = sql_db.selectTrackById(self.track2._id)
        self.assertEqual(track2_from_db, self.track2)

    def testInsertAndSelectMultipleTracks(self):

        # make the database (store in memory only)
        store_in_memory = True
        sql_db = sqldatabase.SQLDatabase(store_in_memory)
        sql_db.createTracksTable()

        # insert tracks into db
        sql_db.insertTracks(self.tracks)

        # select all tracks and check the result
        # tracks won't necessarily come back in the same order, so just check they're in the list
        tracks_from_db = sql_db.selectAllTracks()
        self.assertEqual(len(tracks_from_db), 2)
        self.assertTrue(self.track1 in tracks_from_db)
        self.assertTrue(self.track2 in tracks_from_db)
    
    def testInsertAndSelectTransitions(self):
        
        # make the database (store in memory only)
        store_in_memory = True
        sql_db = sqldatabase.SQLDatabase(store_in_memory)
        sql_db.createTransitionsTable()

        # insert transitions into db
        sql_db.insertTransition(self.transition1)

        # select all transitions and check the result
        # tracks won't necessarily come back in the same order, so just check they're in the list
        transitions_from_db = sql_db.selectAllTransitions()
        self.assertEqual(len(transitions_from_db), 1)
        self.assertTrue(self.transition1 in transitions_from_db)
        # self.assertTrue(self.track2 in tracks_from_db)
        
        # select first transition and check the result
        transition1_from_db = sql_db.selectTransitionById(self.transition1._id)        
        self.assertEqual(transition1_from_db, self.transition1)

        # select second transition and check the result
        # track2_from_db = sql_db.selectTrackById(self.track2._id)
        # self.assertEqual(track2_from_db, self.track2)
    
if __name__ == "__main__":
    unittest.main()