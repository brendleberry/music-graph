import sys
import glob
import os.path

from mutagen.id3 import ID3

import keyhelperfunctions
import track
import sqldatabase as sqldb

class TrackLoader:

    def loadTrackFromFile(self, file:str):
        # can't do anything if the file doesn't exist
        if not os.path.exists(file):
            return None

        id3 = ID3()
            
        try:
            id3.load(file)
        except:
            print(f"{file} has no ID3 tag")

        key = keyhelperfunctions.ConvertKey(str(id3.get("TKEY")))
        artist = str(id3.get("TPE1"))
        title = str(id3.get("TIT2"))

        return track.Track.generateId(title, artist, key) if key != None else None

    def loadTracksFromFolder(self, folder:str):   
        # can't do anything if the folder doesn't exist
        if not os.path.exists(folder):
            return []

        # load every mp3 in "folder" and all its subdirectories
        tracks = [self.loadTrackFromFile(f) for f in glob.iglob(f"{folder}/**/*.mp3", recursive=True)]

        # filter out the ones that failed to load
        return list(filter(None, tracks))